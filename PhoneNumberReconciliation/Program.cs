﻿using System.Configuration;
using CsvHelper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using BOG.Framework.Extensions;
using Twilio.Rest.Api.V2010.Account;

namespace PhoneNumberReconciliation
{
	public partial class Program
	{
		// CHANGE THIS: DEV | STAGE | PROD ... controls the Twilio account to work with.
		const string PROCESSING_PLATFORM = "DEV";

		static string storagePath;
		static string AccountSid;
		static string AuthToken;
		static string PhoneInfoSetSourceFile;
		static string PhoneInfoSetFile;

		static JsonSerializerSettings jsonSettings = new JsonSerializerSettings
		{
			Formatting = Formatting.Indented,
			DateFormatHandling = DateFormatHandling.MicrosoftDateFormat,
			DateParseHandling = DateParseHandling.None,
			NullValueHandling = NullValueHandling.Include
		};

		static List<PhoneInfo> phoneInfoList = new List<PhoneInfo>();

		static void Main(string[] args)
		{
			AccountSid = ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.Common.AccountSid"];
			AuthToken = ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.Common.AuthToken"];

			Twilio.TwilioClient.Init(username: AccountSid, password: AuthToken);

			storagePath = ConfigurationManager.AppSettings["StoragePath"].ResolvePathPlaceholders();
			PhoneInfoSetSourceFile = Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.Program.PhoneInfoSetSourceFile"]);
			PhoneInfoSetFile = Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.Program.PhoneInfoSetFile"]);

			try
			{
				if (!Directory.Exists(storagePath)) Directory.CreateDirectory(storagePath);
				if (!Directory.Exists(storagePath)) throw new IOException($"Could not create storage folder: {storagePath}");

				// An existing JSON file takes priority as the phone numbers to process
				if (File.Exists(PhoneInfoSetSourceFile))
				{
					// If a TSV file is found, a collection PhoneInfo objects is build from each row.
					// The columns need to have Number and SID values.
					Console.WriteLine($"Found PhoneInfo source set file... loading from: {PhoneInfoSetSourceFile}");
					Console.WriteLine($"Building PhoneInfo set file: {PhoneInfoSetFile}");
					LoadPhoneInfoListFromTsvFile();
					SavePhoneInfoSetToFile();
				}
				else if (File.Exists(PhoneInfoSetFile))
				{
					Console.WriteLine($"Found PhoneInfo set file... loading from: {PhoneInfoSetFile}");
					LoadPhoneInfoSetFromFile();
				}

				/*
					Ensure that the June 2019 security requirements (TLS version >= 1.2) are met.
				*/
				//SecurityCheck();

				/*** Uncomment the function you wish to perform with the numbers ***/
				/*** Only one function should occur per execution                ***/

				/* 
				   ShowUsageRecords returns a JSON blog containing a summary of the account usage
				*/

				//ShowUsageRecords();

				/* 
				  BuildTwilioPhoneNumberList builds a list of phone numbers which are on rent from the Twilio account SID
				*/

				BuildTwilioPhoneNumberList();

				/*
				  CheckPhoneNumbersAgainstTwilio queries specific phone numbers against the Twilio account, and builds an inventory list
				  The list of numbers to check is loaded above into the phoneInfoList object
				*/
				//CheckPhoneNumbersAgainstTwilio();

				/*
				  RemoveOrphanedPhoneNumbers issues a release command for specific phone numbers.  This takes the number out of on-rent status.
				  The list of numbers to release is loaded above into the phoneInfoList object.
				  Be careful: this is the one method which changes information at Twilio.
				*/
				//RemoveOrphanedPhoneNumbers();
			}
			catch (Exception err)
			{
				Console.WriteLine();
				Console.WriteLine("Stopping...");
				Console.WriteLine();
				Console.WriteLine(err.ToString());
			}

			Console.WriteLine();
			Console.WriteLine("Done ... press ENTER");
			Console.ReadLine();
		}

		#region Helper methods
		/// <summary>
		/// Loads an existing set of PhoneInfo objects that are pending or continunig processing.
		/// </summary>
		static void LoadPhoneInfoSetFromFile()
		{
			using (StreamReader sr = new StreamReader(PhoneInfoSetFile))
			{
				phoneInfoList = new List<PhoneInfo>(JsonConvert.DeserializeObject<List<PhoneInfo>>(sr.ReadToEnd(), jsonSettings));
			}
		}

		/// <summary>
		/// Saves an existing set of PhoneInfo objects to a text file.  Done occsassionally during processing and at completion.
		/// </summary>
		static void SavePhoneInfoSetToFile()
		{
			using (StreamWriter sw = new StreamWriter(PhoneInfoSetFile))
			{
				sw.Write(JsonConvert.SerializeObject(phoneInfoList, jsonSettings));
			}
		}

		/// <summary>
		/// Loads a list of PhoneInfo objects from a TSV file.  The file only needs Number and SID for columns, with headers, but can have 
		/// any of the other properties as well.  Note: this is not CSV: column separator is a tab.
		/// </summary>
		static void LoadPhoneInfoListFromTsvFile()
		{
			phoneInfoList.Clear();
			var csvConfig = new CsvHelper.Configuration.Configuration
			{
				Delimiter = "\t",
				HasHeaderRecord = true,
				IgnoreQuotes = false,
				MemberTypes = CsvHelper.Configuration.MemberTypes.Properties,
				Quote = '"',
				TrimOptions = CsvHelper.Configuration.TrimOptions.None,
				HeaderValidated = null,
				MissingFieldFound = null
			};
			using (var reader = new StreamReader(PhoneInfoSetSourceFile))
			{
				var csv = new CsvReader(reader, csvConfig);
				foreach (var phoneInfo in csv.GetRecords<PhoneInfo>())
				{
					phoneInfoList.Add(new PhoneInfo
					{
						Number = phoneInfo.Number,
						SID = phoneInfo.SID,
						Status = phoneInfo.Status,
						Message = phoneInfo.Message,
					});
				}
			}
		}
		#endregion
	}
}
