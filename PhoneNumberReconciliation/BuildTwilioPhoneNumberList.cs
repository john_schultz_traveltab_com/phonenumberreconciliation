﻿using BOG.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace PhoneNumberReconciliation
{
	public partial class Program
	{
		static string fileException;
		static string fileSingleAnswer;
		static string filePhoneNumberListTxt;
		static string filePhoneNumberListJson;

		static void BuildTwilioPhoneNumberList()
		{
			Console.WriteLine("BuildTwilioPhoneNumberList... starts");
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			fileException = Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.BuildTwilioPhoneNumberList.fileException"]);
			fileSingleAnswer = Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.BuildTwilioPhoneNumberList.fileSingleAnswer"]);
			filePhoneNumberListTxt = string.Format(Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.BuildTwilioPhoneNumberList.filePhoneNumberListTxt"]), DateTime.Now);
			filePhoneNumberListJson = string.Format(Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.BuildTwilioPhoneNumberList.filePhoneNumberListJson"]), DateTime.Now);

			if (File.Exists(fileException)) File.Delete(fileException);
			if (File.Exists(fileSingleAnswer)) File.Delete(fileSingleAnswer);

			var collected = new List<PhoneAvailableInfo>();

			try
			{
				var options = new ReadIncomingPhoneNumberOptions
				{
					PathAccountSid = "AP5d9b31b7ba004420a1ec7dd8aec985c4",
				};
				Console.WriteLine("Query Twilio...");
				var incomingPhoneNumbers = IncomingPhoneNumberResource.Read();
				Console.WriteLine("Build local list...");
				var timestamp = DateTime.Now;
				foreach (var p in incomingPhoneNumbers)
				{
					collected.Add(new PhoneAvailableInfo
					{
						PhoneNumber = p.PhoneNumber.ToString(),
						FriendlyName = p.FriendlyName,
						Sid = p.Sid,
						DateCreated = p.DateCreated ?? DateTime.MinValue,
						DateUpdated = p.DateUpdated ?? DateTime.MinValue,
						DateSnapshotAcquired = timestamp
					});
				}

				Console.Write($"Got: {collected.Count} items ... ");
				Console.WriteLine("Build text file...");
				using (var sw = new StreamWriter(filePhoneNumberListTxt))
				{
					sw.WriteLine("PhoneNumber\tFriendlyName\tSid\tCreatedDate\tModifiedDate\tCaptured");
					foreach (var c in collected)
					{
						sw.WriteLine($"{c.PhoneNumber}\t{c.FriendlyName}\t{c.Sid}\t{c.DateCreated}\t{c.DateUpdated}\t{c.DateSnapshotAcquired.ToString("s")}");
					}
				}

				Console.WriteLine("Build json file...");
				using (var sw = new StreamWriter(filePhoneNumberListJson, true))
				{
					sw.Write(JsonConvert.SerializeObject(collected, new JsonSerializerSettings
					{
						Formatting = Newtonsoft.Json.Formatting.Indented,
						MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
						DateParseHandling = DateParseHandling.None,
						DateFormatHandling = DateFormatHandling.IsoDateFormat
					}));
				}
			}
			catch (Exception err)
			{
				Console.WriteLine($"Exception: {err.Message}");
				Console.WriteLine($"For details, see {fileException}");
				using (var sw = new StreamWriter(fileException, true))
				{
					sw.WriteLine(DetailedException.WithMachineContent(ref err));
				}
			}
			stopwatch.Stop();
			Console.WriteLine($"BuildTwilioPhoneNumberList... ends... elapsed: {stopwatch.Elapsed.ToString()}");
			Console.WriteLine();
		}
	}
}
