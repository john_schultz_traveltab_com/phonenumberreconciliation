﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace PhoneNumberReconciliation
{
	public partial class Program
	{
		static string filePhoneNumberExplictReleaseLog;
		static string filePhoneNumberExplictRelease;

		static void RemoveOrphanedPhoneNumbers()
		{
			Console.WriteLine("RemoveOrphanedPhoneNumbers");
			var stopwatch = new Stopwatch();
			Console.WriteLine("Starting...");
			stopwatch.Start();

			var now = DateTime.Now;
			filePhoneNumberExplictReleaseLog = string.Format(Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.RemoveOrphanedPhoneNumbers.filePhoneNumberExplictReleaseLog"]), now);
			filePhoneNumberExplictRelease = string.Format(Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.RemoveOrphanedPhoneNumbers.filePhoneNumberExplictRelease"]), now);

			using (var sw = new StreamWriter(filePhoneNumberExplictReleaseLog))
			{
				int updateCounter = 0;
				var message = string.Empty;

				foreach (var phoneInfo in phoneInfoList)
				{
					if (updateCounter > 0 && updateCounter % 5 == 0) SavePhoneInfoSetToFile();

					switch (phoneInfo.Status)
					{
						case PhoneInfo.State.Pending:
							break;

						case PhoneInfo.State.Complete:
						case PhoneInfo.State.Errored:
						case PhoneInfo.State.NotFound:
						case PhoneInfo.State.Found:
							message = $"Skip: {phoneInfo.Number}, State {phoneInfo.Status.ToString()}";
							Console.WriteLine(message);
							sw.WriteLine(message);
							continue;

						case PhoneInfo.State.InProcess:
							message = $"Retry: {phoneInfo.Number}, State {phoneInfo.Status.ToString()}";
							Console.WriteLine(message);
							sw.WriteLine(message);
							break;

						default:
							SavePhoneInfoSetToFile();
							throw new Exception($"Entry: {phoneInfo.Number}, has an unknown processing state {phoneInfo.Status.ToString()}");
					}

					updateCounter++;
					phoneInfo.Status = PhoneInfo.State.InProcess;
					message = $"Remove: {phoneInfo.Number}, SID: {phoneInfo.SID} ... ";
					Console.Write(message);
					sw.WriteLine(message);
					sw.Write("1) Validate: ");
					try
					{
						var incomingPhoneNumber = IncomingPhoneNumberResource.Fetch(phoneInfo.SID);
						phoneInfo.Number = incomingPhoneNumber.PhoneNumber.ToString();
					}
					catch (Twilio.Exceptions.ApiException apiErr)
					{
						if (apiErr.Message.IndexOf("was not found") == -1)
						{
							throw new Exception("Error querying a phone number at the Twilio API", apiErr);
						}
						sw.WriteLine("not found on Twilio");
						Console.WriteLine("not found on Twilio");
						phoneInfo.Message = "Not found on Twilio";
						phoneInfo.Status = PhoneInfo.State.NotFound;
						continue;
					}
					catch (Exception err)
					{
						sw.WriteLine($"error: {err.Message}");
						Console.WriteLine($"error: {err.Message}");
						phoneInfo.Message = ($"error: {err.Message}");
						phoneInfo.Status = PhoneInfo.State.Errored;
						continue;
					}

					sw.Write("2) Remove: ");

					if (IncomingPhoneNumberResource.Delete(phoneInfo.SID))
					{
						sw.WriteLine("success");
						Console.WriteLine("success");
						phoneInfo.Message = null;
						phoneInfo.Status = PhoneInfo.State.Complete;
					}
					else
					{
						sw.WriteLine("FAILED");
						Console.WriteLine("FAILED");
						phoneInfo.Status = PhoneInfo.State.Errored;
						phoneInfo.Message = "Removal FAILED";
					}
				}
				SavePhoneInfoSetToFile();

				stopwatch.Stop();
				Console.WriteLine($"Elapsed run time: {stopwatch.Elapsed.ToString()}");
				Console.WriteLine("RemoveOrphanedPhoneNumbers .. ends");
				Console.WriteLine();
			}
		}
	}
}
