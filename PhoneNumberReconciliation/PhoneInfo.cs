﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace PhoneNumberReconciliation
{
	[JsonObject]
	public class PhoneInfo
	{
		public enum State : int { Pending, NotFound, Found, InProcess, Errored, Complete }

		[JsonRequired]
		public string Number { get; set; } = "Indeterminate";

		[JsonRequired]
		public string SID { get; set; } = "Indeterminate";

		[JsonConverter(typeof(StringEnumConverter))]
		public State Status { get; set; } = State.Pending;

		public string Message { get; set; } = null;
	}	
}
