﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Twilio;
using Twilio.Rest.Api.V2010.Account;

namespace PhoneNumberReconciliation
{
	public partial class Program
	{
		static string fileOnTwilio;

		static void CheckPhoneNumbersAgainstTwilio()
		{
			Console.WriteLine("CheckPhoneNumbersAgainstTwilio");

			var stopwatch = new Stopwatch();
			Console.WriteLine("Starting...");
			stopwatch.Start();

			fileOnTwilio = string.Format(Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.CheckPhoneNumbersAgainstTwilio.fileOnTwilio"]), DateTime.Now);

			if (File.Exists(fileOnTwilio)) File.Delete(fileOnTwilio);

			int processedCount = 0;
			int processedPercentageDisplayed = -1;
			using (var sw = new StreamWriter(fileOnTwilio))
			{
				sw.WriteLine("Sid\tResult");
				foreach (var phoneInfo in phoneInfoList)
				{
					processedCount++;
					int processedPercentage = (100 * processedCount) / phoneInfoList.Count;
					if (processedPercentage != processedPercentageDisplayed)
					{
						SavePhoneInfoSetToFile();
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine(".");
						Console.WriteLine($"==== {processedPercentage}% =======================================");
						processedPercentageDisplayed = processedPercentage;
					}
					if (phoneInfo.Status == PhoneInfo.State.Complete) continue;

					Console.Write($"{processedCount} of {phoneInfoList.Count}: {phoneInfo.SID} for {phoneInfo.Number} ... ");
					var result = "indeterminate";

					try
					{
						var incomingPhoneNumber = IncomingPhoneNumberResource.Fetch(phoneInfo.SID);
						result = "found";
						Console.WriteLine($"found.");
						phoneInfo.Status = PhoneInfo.State.Found;
					}
					catch (Twilio.Exceptions.ApiException apiErr)
					{
						if (apiErr.Message.IndexOf("was not found") == -1)
						{
							throw new Exception("Error querying a phone number at the Twilio API", apiErr);
						}
						result = "not found";
						Console.WriteLine($"not found.");
						phoneInfo.Status = PhoneInfo.State.NotFound;
					}
					catch (Exception e)
					{
						result = $"Exception: {e.Message}";
						Console.WriteLine("Exception: {e.Message}");
						phoneInfo.Status = PhoneInfo.State.Errored;
						phoneInfo.Message = e.Message;
					}
					sw.WriteLine($"{phoneInfo.SID}\t{result}");
				}
			}
			SavePhoneInfoSetToFile();
			Console.WriteLine("CheckPhoneNumbersAgainstTwilio .. ends");
			Console.WriteLine();
		}
	}
}
