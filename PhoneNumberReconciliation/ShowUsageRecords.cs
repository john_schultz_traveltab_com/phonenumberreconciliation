﻿using Newtonsoft.Json;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using Twilio;
using Twilio.Rest.Api.V2010;

namespace PhoneNumberReconciliation
{
	public partial class Program
	{
		static string fileUsageResult;

		/// <summary>
		/// Shows total costs associated with certain services.
		/// </summary>
		static void ShowUsageRecords()
		{
			Console.WriteLine("ShowUsageRecords");
			var stopwatch = new Stopwatch();
			Console.WriteLine("Starting...");
			stopwatch.Start();

			fileUsageResult = string.Format(Path.Combine(storagePath, ConfigurationManager.AppSettings[$"{PROCESSING_PLATFORM}.ShowUsageRecords.fileUsageResult"]), DateTime.Now);
			
			if (File.Exists(fileUsageResult)) File.Delete(fileUsageResult);

			var result = AccountResource.Fetch();
			using (var sw = new StreamWriter(fileUsageResult))
			{
				sw.Write(JsonConvert.SerializeObject(result, jsonSettings));
			}
			Console.WriteLine("ShowUsageRecords.. ends");
			Console.WriteLine();
		}
	}
}
