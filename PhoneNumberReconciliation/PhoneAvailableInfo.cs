﻿using Newtonsoft.Json;
using System;

namespace PhoneNumberReconciliation
{
	[JsonObject]
	public class PhoneAvailableInfo
	{
		[JsonRequired]
		public string PhoneNumber { get; set; } = "Indeterminate";

		[JsonRequired]
		public string Sid { get; set; } = "Indeterminate";

		[JsonRequired]
		public string FriendlyName { get; set; } = "Indeterminate";

		[JsonRequired]
		public DateTime DateCreated { get; set; } = DateTime.MinValue;

		[JsonRequired]
		public DateTime DateUpdated { get; set; } = DateTime.MinValue;

		[JsonRequired]
		public DateTime DateSnapshotAcquired { get; set; } = DateTime.Now;
	}
}
