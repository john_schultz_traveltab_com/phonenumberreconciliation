﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Twilio.Http;

namespace PhoneNumberReconciliation
{
	public partial class Program
	{
		static void SecurityCheck()
		{
			Console.WriteLine("SecurityCheck ..");
			try
			{
				HttpClient client = new SystemNetHttpClient();
				Request request = new Request(HttpMethod.Get, "https://api.twilio.com:8443");
				Response response = client.MakeRequest(request);
				Console.Write($"response: {response.StatusCode.ToString()}");
				Console.Write(response.Content);
			}
			catch (Exception err)
			{
				Console.WriteLine($"exception: {err.Message}");
			}

			Console.WriteLine("SecurityCheck .. ends");
			Console.WriteLine();
		}
	}
}

