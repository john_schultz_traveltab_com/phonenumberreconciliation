# PhoneNumberReconciliation

A one-off tool to examine phone numbers assigned by Twilio to TravelTab for the various deployment platforms: Dev, QA, Stage, Training, Prod.  

The app.config file will need the SID and AuthToken values for the environment you are working with on Twilio.  These can be found in the 
Azure portal for WEB06 (Rental Agreement service) or WEB02 (WCF Services).

Usage:
By default all the method calls are commented out.  Uncomment the method you want.  It is best to only do one method at a time.

- ShowUsageRecords returns a JSON blob containing a summary of the account usage
- BuildTwilioPhoneNumberList builds a list of phone numbers which are on rent from the Twilio account SID
- CheckPhoneNumbersAgainstTwilio queries specific phone numbers against the Twilio account, and builds an inventory list
- RemoveOrphanedPhoneNumbers issues a release command for specific phone numbers.  This takes the number out of on rent, assuming it is not in use.

Reference TNG-3959 for examples.
